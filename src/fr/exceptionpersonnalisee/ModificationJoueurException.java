package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class ModificationJoueurException extends Exception {
	String valeurImpossible;

	public ModificationJoueurException() {
	}

	public ModificationJoueurException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param message
	 * @param valeurImpossible
	 */
	public ModificationJoueurException(String message, String valeurImpossible) {
		super(message);
		this.valeurImpossible = valeurImpossible;
	}
}
