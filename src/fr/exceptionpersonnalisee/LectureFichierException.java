package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class LectureFichierException extends Exception {
	String nomFichier;

	public LectureFichierException() {
	}

	public LectureFichierException(String message) {
		super(message);
	}

	public LectureFichierException(String message, String nomFichier) {
		super(message);
		this.nomFichier = nomFichier;
	}
}
