package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class ModificationFichierChargerException extends Exception {
	String valeurImpossible;

	public ModificationFichierChargerException() {
	}

	public ModificationFichierChargerException(String message) {
		super(message);
	}

	public ModificationFichierChargerException(String message, String valeurImpossible) {
		super(message);
		this.valeurImpossible = valeurImpossible;
	}
}
