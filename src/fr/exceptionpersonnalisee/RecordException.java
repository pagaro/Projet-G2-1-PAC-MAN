package fr.exceptionpersonnalisee;

@SuppressWarnings("serial")
public class RecordException extends Exception {
	String valeurImpossible;

	public RecordException() {
	}

	public RecordException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param message
	 * @param valeurImpossible
	 */
	public RecordException(String message, String valeurImpossible) {
		super(message);
		this.valeurImpossible = valeurImpossible;
	}
}
