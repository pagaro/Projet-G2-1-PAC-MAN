package fr.pacman.zonedejeux.decor;

import fr.exceptionpersonnalisee.ModificationNiveauException;

/**
 * Cette classe definis un Niveau qui est composer d'un nom, ID, et contient une map de jeu
 * ainsi qu'un record associer en fonction du temps qu'a mis le joueur pour la terminer.
 * Un nouveau niveau peut contenir une map existant deja dans un autre niveau.
 * @author mathis
 */
public class Niveau {
	private final String nomFichier;
	private final String id;
	private String nom;
	private Map map;
	private Record record;

	/**
	 * Constructeur Niveau
	 * 
	 * @param nomFichier
	 * @param id
	 * @param nom
	 * @param map
	 * @param record
	 */
	public Niveau(String nomFichier, String id, String nom, Map map, Record record) {
		super();
		this.nomFichier = nomFichier;
		this.id = id;
		this.nom = nom;
		this.map = map;
		this.record = record;
	}

	/**
	 * set le nom du niveau
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * set la map du niveau.
	 * @param map
	 */
	public void setMap(Map map) {
		this.map = map;
	}

	/**
	 * Cette methode permet de set le nouveau record d'un niveau si il est superieur que le precedent.
	 * 
	 * @param record
	 * @throws ModificationNiveauException
	 */
	public void setRecordNiveau(Record record) throws ModificationNiveauException {

		if (record == null) {
			throw new ModificationNiveauException("Erreur : record Niveau null");
		}

		if (this.record == null) {
			this.record = record;
		} else {
			this.record = this.record.meilleurRecord(record);
		}
		
	}
	
	/**
	 * @return le nom du fichier
	 */
	public String getNomFichier() {
		return nomFichier;
	}

	/**
	 * @return id du niveau 
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return le nom du niveau 
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return la map du niveau
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * @return le record actuel du niveau
	 */
	public Record getRecordNiveau() {
		return record;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((nomFichier == null) ? 0 : nomFichier.hashCode());
		result = prime * result + ((record == null) ? 0 : record.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Niveau other = (Niveau) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (nomFichier == null) {
			if (other.nomFichier != null)
				return false;
		} else if (!nomFichier.equals(other.nomFichier))
			return false;
		if (record == null) {
			if (other.record != null)
				return false;
		} else if (!record.equals(other.record))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Niveau \n	nomFichier=" + nomFichier + "\n	id=" + id + "\n	nom=" + nom + "\n	map=	" + map
				+ "\n	recordNiveau=" + record + "\n";
	}

}
