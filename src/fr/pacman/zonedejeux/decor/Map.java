package fr.pacman.zonedejeux.decor;

import java.util.Collection;

import fr.exceptionpersonnalisee.BlockException;
import fr.exceptionpersonnalisee.ModificationNiveauException;

/**
 * Cette class permet de fabriquer des map. Les map sont constitue d'un
 * collection de 775 Block. Un map ne peut pas mits dans la class si elle ne
 * repond a aux criteres. Soit une map est valide soit elle est null. Une fois
 * qu'elle est valide elle ne peut plus redevenir null.
 * 
 * @author gillian
 *
 */
public class Map {
	public final static int longueurMap = 31;
	public final static int largeurMap = 25;

	private Collection<Block> mapCollection;

	/**
	 * Constructeur
	 */
	public Map() {
		this.mapCollection = null;
	}

	public Map(Collection<Block> mapMap) {
		this();
		this.mapCollection = mapMap;
	}

	/**
	 * Permet de verifier avant de mettre la nouvelle map dans mapCollection si la
	 * map est valide.
	 * 
	 * @param nouvelleMap La nouvelle map que l'on souhaite mettre a la place.
	 * @throws ModificationNiveauException
	 */
	public void setMapCollection(Collection<Block> nouvelleMap) throws ModificationNiveauException {

		if (!mapEstValide(nouvelleMap)) {
			throw new ModificationNiveauException("Erreur : changement map impossible (nouvelle map pas valide)");
		}
		this.mapCollection = nouvelleMap;
		try {
			this.orientationMurMap();
		} catch (BlockException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Permet de verifier si la map correspond a plusieur critere indispansable pour
	 * le bon fonctionnement d'un partie.
	 * 
	 * @param mapCollection La map que l'on souhaite verifier.
	 * @return retourne vrai ou faux
	 */
	public static boolean mapEstValide(Collection<Block> mapCollection) {

		if (mapCollection == null) {
			return false;
		}
		if (mapCollection.size() != Map.largeurMap * Map.longueurMap) {
			return false;
		}
		if (!verificationBordureClos(convertirColectionToTab(mapCollection))) {
			return false;
		}
		if (!mapContientIlotCentral(convertirColectionToTab(mapCollection))) {
			return false;
		}

		if (!mapEstContinue(convertirColectionToTab(mapCollection))) {
			return false;
		}

		return true;
	}

	/**
	 * Permet de retourner un tableau de 3*3. le tableau a comme element central le
	 * block avec les coordone� pass� en parametres. les block autour sont les
	 * voisins dans la carte. les block qui entour un 1 block de distance le block
	 * demander.
	 * 
	 * @param x la coordon� X du block
	 * @param y la coordon� Y du block
	 * @return retourne un tableau 3*3 de block
	 */
	public Block[][] voisin(int x, int y) {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				voisin[i][j] = map[y + i - 1][x + j - 1];
			}
		}

		return voisin;
	}

	/**
	 * Permet de retourner un tableau de 3*3. le tableau a comme element central le
	 * block avec les coordone� pass� en parametres. les block autour sont les
	 * voisins dans la carte. les block qui entour un 1 block de distance le block
	 * demander.
	 * 
	 * @param x la coordon� X du block
	 * @param y la coordon� Y du block
	 * @return retourne un tableau 3*3 de block
	 */
	public static Block[][] voisin(int x, int y, Collection<Block> mapCollection) {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab(mapCollection);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				voisin[i][j] = map[y + i - 1][x + j - 1];

			}

		}

		return voisin;
	}
	
	/**
	 * @param x
	 * @param y
	 * @return le type du block voisin d'en Haut.
	 * @throws BlockException
	 */
	public Block[][] voisinHaut(int x, int y) throws BlockException {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab();
		for (int i = 0; i < 3; i++) {

			voisin[0][i] = map[largeurMap - 2][x + i - 1];

		}

		return voisin;
	}

	/**
	 * @param x
	 * @param y
	 * @return le type du block voisin d'en Bas.
	 * @throws BlockException
	 */
	public Block[][] voisinBas(int x, int y) throws BlockException {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab();
		for (int i = 0; i < 3; i++) {

			voisin[2][i] = map[1][x + i - 1];

		}

		return voisin;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return le type du block voisin de Gauche.
	 * @throws BlockException
	 */
	public Block[][] voisinGauche(int x, int y) throws BlockException {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab();
		for (int i = 0; i < 3; i++) {

			voisin[i][0] = map[y + i - 1][longueurMap - 2];

		}

		return voisin;
	}

	/**
	 * @param x
	 * @param y
	 * @return le type du block voisin de Droite.
	 * @throws BlockException
	 */
	public Block[][] voisinDroite(int x, int y) throws BlockException {
		Block[][] voisin = new Block[3][3];
		Block[][] map = convertirColectionToTab();
		for (int i = 0; i < 3; i++) {

			voisin[i][2] = map[y + i - 1][1];

		}

		return voisin;
	}

	/**
	 * @return vrai ou faux si il y a un block siter en Haut a droite du couloir.
	 * @throws BlockException
	 */
	public boolean voisinHautDroite() throws BlockException {

		Block[][] map = convertirColectionToTab();
		if (map[1][longueurMap - 2].getTypeblock() == Block.typeCouloir) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @return vrai ou faux si il y a un block situer en Haut a Gauche du couloir.
	 * @throws BlockException
	 */
	public boolean voisinHautGauche() throws BlockException {

		Block[][] map = convertirColectionToTab();
		if (map[1][1].getTypeblock() == Block.typeCouloir) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @return vrai ou faux si il y a un block situeren bas a droite du couloir.
	 * @throws BlockException
	 */
	public boolean voisinBasDroite() throws BlockException {

		Block[][] map = convertirColectionToTab();
		if (map[largeurMap - 2][longueurMap - 2].getTypeblock() == Block.typeCouloir) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @return vrai ou faux, si il y a un block situer en Bas a gauche du couloir.
	 * @throws BlockException
	 */
	public boolean voisinBasGauche() throws BlockException {

		Block[][] map = convertirColectionToTab();
		if (map[largeurMap - 2][1].getTypeblock() == Block.typeCouloir) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Permet de verifier si la map en bien entourer de mur "0".
	 * 
	 * @param mapBlock La map en forme de tableau que l'on souhaite verifier
	 * @return retourne vrai ou faux
	 */
	private static boolean verificationBordureClos(Block[][] mapBlock) {

		for (int i = 0; i < Map.largeurMap; i++) {
			if (mapBlock[i][0].getTypeblock() != Block.typeMur
					|| mapBlock[i][Map.longueurMap - 1].getTypeblock() != Block.typeMur) {
				return false;
			}
		}

		for (int longeur = 0; longeur < Map.longueurMap; longeur++) {
			if (mapBlock[0][longeur].getTypeblock() != Block.typeMur
					|| mapBlock[Map.largeurMap - 1][longeur].getTypeblock() != Block.typeMur) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Permet de verifier si un ilot central est bien present dans la map. L'ilot
	 * est indispanseble pour le spawn des personnage.
	 * 
	 * @param mapBlock La map en forme de tableau que l'on souhaite verifier
	 * @return retourne vrai ou faux
	 */
	private static boolean mapContientIlotCentral(Block[][] mapBlock) {
		for (int i = (Map.longueurMap / 2) - 1; i <= (Map.longueurMap / 2) + 1; i++) {
			if (mapBlock[((Map.largeurMap) / 2)][i].getTypeblock() != Block.typeSpawn) {
				return false;
			}
		}

		for (int i = (Map.longueurMap / 2) - 2; i <= (Map.longueurMap / 2) + 2; i++) {
			if (i != 15) {
				if (mapBlock[((Map.largeurMap) / 2) - 1][i].getTypeblock() != Block.typeMur
						|| mapBlock[((Map.largeurMap) / 2) + 1][i].getTypeblock() != Block.typeMur) {
					return false;
				}
			} else if (mapBlock[((Map.largeurMap) / 2) - 1][i].getTypeblock() != Block.typeSpawn
					|| mapBlock[((Map.largeurMap) / 2) + 1][i].getTypeblock() != Block.typeMur) {
				return false;
			}

		}

		if (mapBlock[Map.largeurMap / 2 - 2][Map.longueurMap / 2].getTypeblock() != Block.typeCouloir) {
			return false;
		}

		if (mapBlock[(Map.largeurMap) / 2][(Map.longueurMap / 2) - 2].getTypeblock() != Block.typeMur
				|| mapBlock[(Map.largeurMap) / 2][(Map.longueurMap / 2) + 2].getTypeblock() != Block.typeMur) {
			return false;
		}

		return true;
	}

	/**
	 * Permet de verifier que le labyrinthe est bien continue. C'est � dire que
	 * l'on peut aller ou l'on veut dans le labyrinthe.
	 * 
	 * @param mapBlock La map en forme de tableau que l'on souhaite verifier
	 * @return retourne vrai ou faux
	 */
	private static boolean mapEstContinue(Block[][] mapBlock) {
		Integer[] depart = new Integer[2];
		depart[0] = 10;
		depart[1] = 15;

		successeur(depart, mapBlock);

		for (int largeur = 0; largeur < Map.largeurMap; largeur++) {
			for (int longeur = 0; longeur < Map.longueurMap; longeur++) {
				if (mapBlock[largeur][longeur].getTypeblock() >= Block.typeCouloir) {
					return false;
				}
			}
		}
		return true;

	}

	/**
	 * Permet de remplacer les block couloir pour voir si a la fin des block couloir
	 * non pas reussie a etres ateint.
	 * 
	 * @param predecesseur appele par recurrence jusqua arriver dans un cul de sac.
	 * @param mapBlock     retourne la map modifier si la map est valide le map ne
	 *                     contiendra plus aucun blockCouloir.
	 */
	private static void successeur(Integer[] predecesseur, Block[][] mapBlock) {
		mapBlock[predecesseur[0]][predecesseur[1]].setTypeblock(Block.typeVerificationMap);

		if (mapBlock[predecesseur[0] + 1][predecesseur[1]].getTypeblock() >= Block.typeCouloir) {
			Integer[] bas = new Integer[2];
			bas[0] = predecesseur[0] + 1;
			bas[1] = predecesseur[1];
			successeur(bas, mapBlock);
		}

		if (mapBlock[predecesseur[0] - 1][predecesseur[1]].getTypeblock() >= Block.typeCouloir) {
			Integer[] haut = new Integer[2];
			haut[0] = predecesseur[0] - 1;
			haut[1] = predecesseur[1];
			successeur(haut, mapBlock);
		}

		if (mapBlock[predecesseur[0]][predecesseur[1] + 1].getTypeblock() >= Block.typeCouloir) {
			Integer[] droite = new Integer[2];
			droite[0] = predecesseur[0];
			droite[1] = predecesseur[1] + 1;
			successeur(droite, mapBlock);
		}

		if (mapBlock[predecesseur[0]][predecesseur[1] - 1].getTypeblock() >= Block.typeCouloir) {
			Integer[] gauche = new Integer[2];
			gauche[0] = predecesseur[0];
			gauche[1] = predecesseur[1] - 1;
			successeur(gauche, mapBlock);
		}

	}

	/**
	 * Permet de convertir un map Collection en tableau a deux dimension pour
	 * certaint usage. il retourne ensuite la map en parametres.
	 * 
	 * @param mapCollection demande en paramet la map que l'on veut modifier
	 * @return retourn le tabrau a deuc dimension constituer de block
	 */
	public static Block[][] convertirColectionToTab(Collection<Block> mapCollection) {
		Block[][] mapTab2Dim = new Block[Map.largeurMap][Map.longueurMap];
		try {
			for (Block i : mapCollection) {
				mapTab2Dim[Math.round(i.getCoordonnerY())][Math.round(i.getCoordonnerX())] = Block
						.newBlock(Math.round(i.getCoordonnerX()), Math.round(i.getCoordonnerY()), i.getTypeblock());
			}
		} catch (BlockException e) {
			mapTab2Dim = null;
		}

		return mapTab2Dim;
	}

	/**
	 * @param mapCollection
	 * @return Un tableau de la map du jeu.
	 */
	public static Block[][] copieMap(Collection<Block> mapCollection) {
		Block[][] mapTab2Dim = new Block[Map.largeurMap][Map.longueurMap];

		for (Block i : mapCollection) {
			mapTab2Dim[Math.round(i.getCoordonnerY())][Math.round(i.getCoordonnerX())] = i;
		}
		return mapTab2Dim;
	}
	
	public Block[][] copieMap() {
		return copieMap(this.mapCollection);
	}

	/**
	 * Convertit le map de la classe ne tableau a deux dimenions.
	 * 
	 * @return retourn le tabrau a deuc dimension constituer de block
	 */
	public Block[][] convertirColectionToTab() {
		return convertirColectionToTab(this.mapCollection);
	}

	public Collection<Block> getMapCollection() {
		return this.mapCollection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mapCollection == null) ? 0 : mapCollection.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Map other = (Map) obj;
		if (mapCollection == null) {
			if (other.mapCollection != null)
				return false;
		} else if (!mapCollection.equals(other.mapCollection))
			return false;
		return true;
	}

	/**
	 * affiche le tableau de la map.
	 */
	@Override
	public String toString() {
		Block[][] mapTab2Dim;

		mapTab2Dim = convertirColectionToTab();

		String string = "";
		for (int j = 0; j < Map.largeurMap; j++) {
			for (int i = 0; i < Map.longueurMap; i++) {
				string = string + String.valueOf(mapTab2Dim[j][i].getTypeblock());
			}
			string = string + "\n		";
		}
		return string;
	}

	/**
	 * d�finis l'orientation des murs de la map
	 * @throws BlockException
	 */
	public void orientationMurMap() throws BlockException {

		Block[][] mapblock = this.copieMap();

		for (int i = 0; i < Map.longueurMap; i++) {
			for (int y = 0; y < Map.largeurMap; y++) {

				mapblock[y][i].OrientationMur(i, y, this);

			}

		}
	}

}
