package fr.pacman.zonedejeux.acteur;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Map;

/**
 * Cette class permet de g�rer le fantome Pockey. Ce fantome se d�place
 * al�atoirement dans la map.
 * 
 * @author Charly
 *
 */

public class Pockey extends Fantome {

	/**
	 * Constructeur de Pockey
	 */
	public Pockey() {
		super(15, 11, new ImageIcon("./data/assets/images/pockey.png").getImage());
	}

	/** 
	 * Cette methode definis la direction de Pockey. 
	 * @return oriantation 
	 */ 
	@Override
	public int changementDirectionAuto(PacMan pacman, Map map) {
		int oriantation = 0;

		oriantation = super.sortieSpawn();

		if (oriantation != 0) {
			return oriantation;
		}

		oriantation = comportementAleatoire(
				super.directionPossible(map.voisin((int) super.coordonnerX, (int) super.coordonnerY)));

		return oriantation;
	}

	/** 
	 * Cette methode definie le comportement aleatoire que prendre Pockey. 
	 * @param posibiliter 
	 * @return le comportement aleatoire que prendra Pockey. 
	 */ 
	private int comportementAleatoire(ArrayList<Integer> posibiliter) {

		if (posibiliter.size() >= 2) {
			switch (super.orientation) {
			case 1:
				posibiliter.remove((Integer) 3);
				break;
			case 2:
				posibiliter.remove((Integer) 4);
				break;
			case 3:
				posibiliter.remove((Integer) 1);
				break;
			case 4:
				posibiliter.remove((Integer) 2);
				break;
			default:
				break;
			}
		}

		return posibiliter.get((int) (Math.random() * (posibiliter.size())));
	}
	
	/** 
	 * @return la texture de Pockey. 
	 */ 
	public Image remetreTexture() {
		return new ImageIcon("./data/assets/images/pockey.png").getImage();
	}
}
