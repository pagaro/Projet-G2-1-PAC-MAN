package fr.pacman.zonedejeux.acteur;

/**
 * Cette classe gere le Personnage PacMan qui est diriger par le joueur, et possede 
 * des oriantation possible ainsi qu'une texture.
 * @author mathe
 */

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Map;

public class PacMan extends Personnage {


	/*
	 * 1gauche 2haut 3droite 4bas 0r
	 */
	private int futureOrientation;
	private boolean modeSuper;
	private int dernierSommet;
	private int dernierOrientationSommet;


	/**
	 * Constructeur
	 * 
	 * @param coordonnerX
	 * @param coordonnerY
	 */
	public PacMan(float coordonnerX, float coordonnerY) {
		super(coordonnerX, coordonnerY, new ImageIcon("./data/assets/images/PAC-MAN.png").getImage(), true, 3, 3);
	}

	/**
	 * la methode permet de definir l'orientation du pacman, il admet Map en
	 * parametre pour verifier si le pacman peut aller sur les cases choisit par
	 * l'utilisateur. Une exception est envoy� si le mur en face du parman est un
	 * mur + gestion des images
	 * 
	 * @param map
	 */
	@Override
	public void deplacement(Map map) {

		if (this.estAuCentre()) {
			ArrayList<Integer> directionP = directionPossible(
					map.voisin((int) super.coordonnerX, (int) super.coordonnerY));

			if (directionP.contains(this.futureOrientation)) {
				this.orientation = this.futureOrientation;
				this.changementImages();
			}
			this.futureOrientation = 0;

			if (!directionP.contains(this.orientation)) {
				this.orientation = 0;
			}

		}
		super.deplacement(map);

	}

	/**
	 * @return La texture a prendre en fonction de la position du PacMan.
	 */
	public Image changementImages() {
		Image img = null;

		switch (this.orientation) {
		case 1:
			img = new ImageIcon("./data/assets/images/PAC-MANgauche.png").getImage();
			break;
		case 2:
			img = new ImageIcon("./data/assets/images/PAC-MANhaut.png").getImage();
			break;
		case 3:
			img = new ImageIcon("./data/assets/images/PAC-MAN.png").getImage();
			break;
		case 4:
			img = new ImageIcon("./data/assets/images/PAC-MANbas.png").getImage();
			break;

		default:
		}
		super.texture = img;
		return img;

	}

	/**
	 * definit la futur orientation lorsqu'elle est appel� par le controle des
	 * touches
	 * 
	 * @param orient
	 */
	public void SetFutureOrientation(int futureOrientation) {
		this.futureOrientation = futureOrientation;
	}

	/**
	 * Enleve un vie.
	 */
	public void perdVie() {
		this.vieRestante--;
	}

	/**
	 * @return Vrai ou Faux si le PacMan est en mode super ou non.
	 */
	public boolean isModeSuper() {
		return modeSuper;
	}
	
	/**
	 * Permet le set le mode super du PacMan
	 * @param modeSuper
	 */
	public void setModeSuper(boolean modeSuper) {
		this.modeSuper = modeSuper;
	}

	/**
	 * @return le dernier sommet par lequel est passer PacMan.
	 */
	public int getDernierSommet() {
		return dernierSommet;
	}

	/*
	 * Met a jour le dernier sommet.
	 */
	public void setUpdaiteDernierSommet(int dernierSommet) {
		this.dernierSommet = dernierSommet;
		this.dernierOrientationSommet = super.orientation;
	}

	/**
	 * @return la dernier oriantation prise par le PacMan.
	 */
	public int getDernierOrientationSommet() {
		return dernierOrientationSommet;
	}

}
