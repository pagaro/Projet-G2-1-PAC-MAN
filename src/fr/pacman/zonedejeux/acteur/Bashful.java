package fr.pacman.zonedejeux.acteur;

/** 
 * Cette class permet de g�rer le fantome Bashful. Ce fantome a un comportement 
 * totalement opposer a PacMan et cherchera toujours a le fuir. 
 *  
 * @author Charly 
 * 
 */ 
 
import java.awt.Image;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Map;

public class Bashful extends Fantome {

	/** 
	 * Constructeur de Bashful. 
	 */ 
	public Bashful() {
		super(16, 12, new ImageIcon("./data/assets/images/bashful.png").getImage());
	}

	/** 
	 * Cette methode definis la direction de Bashful. 
	 * @return oriantation 
	 */ 
	@Override
	public int changementDirectionAuto(PacMan pacman, Map map) {
		int oriantation = 0;

		oriantation = super.sortieSpawn();

		if (oriantation != 0) {
			return oriantation;
		}

		oriantation = comportementOppose(pacman,
				super.directionPossible(map.voisin((int) super.coordonnerX, (int) super.coordonnerY)));

		return oriantation;
	}
	
	/** 
	 * @return la texture de Bashful. 
	 */ 
	public Image remetreTexture() {
		return new ImageIcon("./data/assets/images/bashful.png").getImage();
	}
}
