package fr.pacman.background.gestion;

import java.util.ArrayList;
import java.util.Collection;



import fr.exceptionpersonnalisee.ModificationFichierChargerException;
import fr.exceptionpersonnalisee.GrosProblemeFichierException;

import fr.exceptionpersonnalisee.RecordException;
import fr.pacman.zonedejeux.decor.Joueur;
import fr.pacman.zonedejeux.decor.Niveau;
import fr.pacman.zonedejeux.decor.Record;

/**
 * FichierCharger est la class central de projet PacMan. Elle regroupe
 * l'ensemble des donn�es lue dans le fichier de Niveau ou de Joueur. Cette
 * class offre des services pour manipuler niveauCharger et joueurConnecter
 * facilement. La seul facon de charger un Niveau ou un Joueur est de passer par
 * cette class en donnant le nom du fichier.
 * 
 * @author gillian
 *
 */
public class FichierCharger {
	private static Collection<Niveau> niveauCharger = new ArrayList<>();;
	private static Joueur joueurConnecter = null;

	/**
	 * Permet de changer le joueurConnecter grace a son nom de fichier. Si un joueur
	 * est dejas connect� il serat deconnect� avant de changer.
	 * 
	 * @param nomfic Le nom du fichier ou est stocker les donn�es en XML du Joueur.
	 * @return retourne le Joueur lu si tous c'est bien pass�.
	 * @throws Exception
	 */
	public static Joueur changementJoueur(String nomfic) throws Exception {
		Joueur joueur = LectureFichierXML.lireJoueur(nomfic);

		if (joueur == null) {
			throw new ModificationFichierChargerException("Erreur joueur est null");
		}

		if (joueurConnecter != null) {
			dechargerJoueur();
		}

		joueurConnecter = joueur;
		return joueur;
	}

	/**
	 * Permet de charger un Niveau grace a son nom de fichier. Si un Niveau avec le
	 * meme Id est dejas charger il sera impossible de charger le nouveau niveau.
	 * 
	 * @param Le nom du fichier ou est stocker les donn�es en XML du Niveau.
	 * @return retourne le Joueur lu si tous c'est bien pass�.
	 * @throws Exception Si le fichier n'a pas reussie a etres lu.
	 */
	public static Niveau chargerNiveau(String nomfichier) throws Exception {
		Niveau niveau = LectureFichierXML.lireNiveau(nomfichier);

		if (niveau == null) {
			throw new ModificationFichierChargerException("Erreur : Niveau null");
		}

		if (getUnNiveau(niveau.getId()) != null) {
			throw new ModificationFichierChargerException("Erreur : impossible ajouter ID dejas charger");
		}

		niveauCharger.add(niveau);

		return niveau;
	}

	/**
	 * Permet de modifier un Niveau deja charger. Le nouveau niveau est pass� en
	 * parametres. L'ancien niveau est remplacer par le nouveau dans le Collection.
	 * Avant d'etres suprimer l'ancien niveau est sauvegarder.
	 * 
	 * @param nouveauNiveau Le nouveau niveau
	 * @throws Exception Si le nouveauNiveau est null ou que le niveau n'est pas
	 *                   present dans la Collection
	 */
	public static void modifierNiveau(Niveau nouveauNiveau) throws Exception {
		if (nouveauNiveau == null) {
			throw new ModificationFichierChargerException("Erreur : Niveau null");
		}

		Niveau ancien = getUnNiveau(nouveauNiveau.getId());

		if (ancien == null) {
			throw new ModificationFichierChargerException("Erreur : impossible modifier niveau pas present");
		}

		sauvegardeNiveau(ancien.getId());
		niveauCharger.remove(ancien);
		niveauCharger.add(nouveauNiveau);
		sauvegardeNiveau(nouveauNiveau.getId());
	}

	/**
	 * Permet de suprimer le JOueur charger. Le Joueur est sauvegarder dans son
	 * fichier avant d'etres supprimer. joueurConnecter devient donc Null.
	 * 
	 * @return retourne le Joueur supprimer
	 * @throws Exception Si la sauvegarde c'est mal passe ou si il est impossible de
	 *                   suprimer le JoueurConnecter
	 */
	public static Joueur dechargerJoueur() throws Exception {
		Joueur joueursupp = sauvegarderJoueur();

		joueurConnecter = null;

		if (joueurConnecter != null) {
			throw new ModificationFichierChargerException("Erreur : impossible de decharger le joueur");
		}

		System.out.println("Dechargement reussi");

		return joueursupp;
	}

	/**
	 * Permet de suprimer un Niveau charger. Le Niveau est sauvegarder dans son
	 * fichier avant d'etres supprimer.
	 * 
	 * @param id L'id du Joueur que l'on veut supprimer.
	 * @return retourne le fichier suprimer.
	 * @throws Exception Si la sauvegarde c'est mal passe ou si il est impossible de
	 *                   suprimer le JoueurConnecter
	 */
	public static Niveau dechargerNiveau(String id) throws Exception {
		Niveau niveausup = getUnNiveau(id);

		if (niveausup == null) {
			throw new ModificationFichierChargerException("Erreur : impossible d�charger niveau pas present");
		}

		sauvegardeNiveau(niveausup.getId());
		niveauCharger.remove(niveausup);

		if (niveauCharger.contains(niveausup)) {
			throw new Exception("Erreur : impossible de suprimer le fichier");
		}

		System.out.println("Supression reussi");
		return niveausup;
	}

	/**
	 * Permet de d'ajouter un record au JoueurConnecter. Le Joueur est sauvegarder
	 * avant et apres l'ajout du record.
	 * 
	 * @param record Le record que l'on veut ajouter a joueurConnecter
	 * @throws Exception Si le record est null. Si il est impossible d'ajouter
	 */
	public static void ajouterRecordJoueur(Record record) throws Exception {
		if (record == null) {
			throw new ModificationFichierChargerException("Erreur : record null");
		}

		sauvegarderJoueur();
		joueurConnecter.ajouterRecord(record);
		sauvegarderJoueur();
	}

	/**
	 * Permet de modifier le record d'un Niveau graces a sont ID. Le Niveau est
	 * sauvegarder avant et apres l'ajout du Record.
	 * 
	 * @param idNiveau L'id du niveau ou l'on veut ajouter le record.
	 * @param record   Le record a ajouter.
	 * @throws Exception Si le record est null ou si le Niveau n'est pas charger
	 *                   dans la NiveauCharger.
	 */
	public static void modifierRecordNiveau(String idNiveau, Record record) throws Exception {
		if (record == null) {
			throw new ModificationFichierChargerException("Erreur : record null");
		}

		Niveau niveau = getUnNiveau(idNiveau);

		if (niveau == null) {
			throw new ModificationFichierChargerException("Erreur : impossible modifier niveau pas present");
		}

		sauvegardeNiveau(niveau.getId());

		niveau.setRecordNiveau(record);
		modifierNiveau(niveau);
		sauvegardeNiveau(niveau.getId());

	}

	/**
	 * Sauvegarde le joueur connecter danss sont fichier. il verifie que la
	 * sauvegarde c'est bien passer et reoturn le joueurConnecter.
	 * 
	 * @return Retourne le Joueur sauvegarder
	 * @throws GrosProblemeFichierException 
	 * @throws Exception Si aucun joueur n'est connecter. Si la sauvegarde c'est mal
	 *                   pass�.
	 */
	private static Joueur sauvegarderJoueur() throws ModificationFichierChargerException, GrosProblemeFichierException {
		if (joueurConnecter == null) {
			throw new ModificationFichierChargerException("Pas de joueur connecte");
		}

		LectureFichierXML.ecrireJoueur(joueurConnecter);

		if (!joueurConnecter.equals(LectureFichierXML.lireJoueur(joueurConnecter.getNomFichier()))) {
			throw new ModificationFichierChargerException("Erreur : sauvegarde different de l'objet");
		}

		System.out.println("Sauvegarde reussi");

		return joueurConnecter;
	}

	/**
	 * Permet de sauvegarder un niveau dans son fichier grace a son id.
	 * 
	 * @param id L'id du Niveau que l'on souhaite sauvegarder.
	 * @return retourne le niveau sauvegarder
	 * @throws GrosProblemeFichierException
	 * @throws RecordException              Si Le niveau n'est pas charger. Si la
	 *                                      sauvegarde c'est mal pass�.
	 */
	private static Niveau sauvegardeNiveau(String id)
			throws ModificationFichierChargerException, GrosProblemeFichierException {
		Niveau niveauSauvegarder = getUnNiveau(id);

		if (niveauSauvegarder == null) {
			throw new ModificationFichierChargerException("Erreur : impossible sauvegarder niveau pas present");
		}

		LectureFichierXML.ecrireNiveau(niveauSauvegarder);

		if (!niveauSauvegarder.equals(LectureFichierXML.lireNiveau(niveauSauvegarder.getNomFichier()))) {
			throw new ModificationFichierChargerException("Erreur : sauvegarde different de l'objet");
		}

		System.out.println("Sauvegarde reussi");

		return niveauSauvegarder;
	}

	/**
	 * Permet de donner le plus grand Id des Niveau connecter.
	 * 
	 * @return retourn le plus grand id trouver ou alors null.
	 */
	public static String plusGrandIdMap() {
		String idPlusGrand = "0";
		for (Niveau i : niveauCharger) {
			if (i.getId().compareTo(idPlusGrand) > 0) {
				idPlusGrand = i.getId();
			}
		}

		return idPlusGrand;
	}

	public static Collection<Niveau> getNiveauCharger() {
		return niveauCharger;
	}

	/**
	 * Permet de retourner un Niveau grace a sont id pass� en parametres.
	 * 
	 * @param id L'id du Niveau que l'on souhaite avoir.
	 * @return retourne le niveau trouver ou null.
	 */
	public static Niveau getUnNiveau(String id) {
		Niveau niveauTrouver = null;
		for (Niveau i : niveauCharger) {
			if (i.getId().compareTo(id) == 0) {
				niveauTrouver = i;
			}
		}

		return niveauTrouver;
	}

	public static Joueur getJoueurConecter() {
		return joueurConnecter;
	}
}
