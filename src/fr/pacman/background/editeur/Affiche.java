package fr.pacman.background.editeur;

/**
 * Cette classe permet l'affiche de l'editeur de map.
 * @author mathe
 */

import fr.pacman.background.gestion.FichierCharger;
import javafx.application.Application;
import javafx.stage.Stage;

public class Affiche extends Application {
	public void start(Stage primaryStage) throws Exception {
		FichierCharger.chargerNiveau("./data/niveau/niveauDijkstra.pac");
		primaryStage = new EditeurMap(FichierCharger.getUnNiveau("N0000000099").getId());
		primaryStage.show();
	}
	public static void main(String[] args) {
		Application.launch();
	}
}
