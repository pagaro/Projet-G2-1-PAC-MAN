package fr.pacman.background.editeur;


import javax.swing.*;


import fr.pacman.background.gestion.FichierCharger;


@SuppressWarnings("serial")
public class EditeurN extends JFrame{
	
	EditeurM panel;
	
	public EditeurN() throws Exception{
		super("PAC MAN");
		setResizable(true);
		panel = new EditeurM(FichierCharger.getUnNiveau("N0000000099").getId());

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(panel);
		
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
