package fr.pacman.background.editeur;

/**
 * Cette classe permet d'editer une nouvelle map.
 * @author mathe
 */

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import fr.exceptionpersonnalisee.BlockException;
import fr.pacman.background.gestion.FichierCharger;
import fr.pacman.background.gestion.NouveauFichier;
import fr.pacman.main.Main;
import fr.pacman.zonedejeux.ElementGraphique;
import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Map;
import fr.pacman.zonedejeux.decor.Niveau;
import fr.pacman.zonedejeux.point.PacGomme;
import fr.pacman.zonedejeux.point.PacGommeFruit;
import fr.pacman.zonedejeux.point.SuperPacGomme;
import javafx.application.Platform;
/**
 * Cette Classe gere l'affiche du menu editeur.
 * @author Malo
 */

@SuppressWarnings("serial")
public class EditeurM extends JPanel implements MouseListener {
	private Map map;
	private Collection<PacGomme> pacgommes;
	private static Niveau niveauJouer;
	private JButton sauvegarde;
	private JButton quitter;
	private JButton mur;
	private JButton couloir;
	private JButton pacGommeFruit;
	private JButton superPacGomme;
	private int selectionBlock;
	private Font fontTexte;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private String erreur = null;
	
	/**
	 * Constructeur de EditeurM, qui charge un niveau existant
	 * @param idNiveau
	 * @throws Exception
	 */

	/**
	 * Constructeur de Editeur Map.
	 * @param idNiveau
	 * @throws Exception
	 */
	public EditeurM(String idNiveau) throws Exception {
		this.setPreferredSize(new Dimension(dim.width, dim.height));
		this.setBackground(Color.black);
		this.setFocusable(true);

		niveauJouer = FichierCharger.getUnNiveau(idNiveau);
		try {
			niveauJouer.getMap().orientationMurMap();
		} catch (BlockException e2) {
			e2.printStackTrace();
		}
		creationPacGomme();
		this.map = niveauJouer.getMap();

		selectionBlock = Block.typeCouloir;
		initialisationBouton();
		this.addMouseListener(this);
		
	}
	/**
	 * Constructeur de EditeurM pour un nouveau niveau
	 * @param idNiveau
	 * @param nouveau
	 * @throws Exception
	 */

	public EditeurM(String nom, boolean nouveau) throws Exception {
		this.setPreferredSize(new Dimension(dim.width, dim.height));
		this.setBackground(Color.black);
		this.setFocusable(true);
		niveauJouer = NouveauFichier.nouveauNiveau(nom);
		try {
			niveauJouer.getMap().orientationMurMap();
		} catch (BlockException e2) {
			e2.printStackTrace();
		}
		creationPacGomme();
		this.map = niveauJouer.getMap();

		selectionBlock = Block.typeCouloir;
		initialisationBouton();
		this.addMouseListener(this);
		
		
	}
	/**
	 * Cette methode affiche les bouton de l'editeur
	 */

	private void initialisationBouton() {
		Container contenaire = new Container();
		new BoxLayout(contenaire, BoxLayout.Y_AXIS);
		mur = new JButton(new ImageIcon("./data/assets/images/1111_0000.jpg"));
		contenaire.add(mur);

		mur.setBounds(100, 100, 100, 30);
		mur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectionBlock = Block.typeMur;
			}
		});

		couloir = new JButton(new ImageIcon("./data/assets/images/0000_0000.jpg"));
		contenaire.add(couloir);
		couloir.setBounds(100, 150, 100, 30);

		couloir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectionBlock = Block.typeCouloir;
			}
		});

		pacGommeFruit = new JButton(new ImageIcon("./data/assets/images/pacgommefruit.png"));
		contenaire.add(pacGommeFruit);
		pacGommeFruit.setBounds(100, 200, 100, 30);

		pacGommeFruit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectionBlock = Block.typePacGommeFruit;

			}
		});

		superPacGomme = new JButton(new ImageIcon("./data/assets/images/superpacgomme.png"));
		contenaire.add(superPacGomme);
		superPacGomme.setBounds(100, 250, 100, 30);

		superPacGomme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectionBlock = Block.typeSuperPacGomme;

			}
		});

		sauvegarde = new JButton("sauvegarde");
		contenaire.add(sauvegarde);
		sauvegarde.setBounds(100, 300, 100, 30);

		sauvegarde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					saveModificationMap();
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});
		quitter = new JButton("Quitter");
		contenaire.add(quitter);
		quitter.setBounds(100, 400, 100, 30);
		quitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		this.add(contenaire);
	}
	
	/**
	 * Cette methode affiche les PacGomme
	 */
	private void creationPacGomme() {
		pacgommes = new ArrayList<>();
		for (Block i : niveauJouer.getMap().getMapCollection()) {
			if (i.getTypeblock() == Block.typePacGommeFruit) {
				pacgommes.add(new PacGommeFruit(i.getCoordonnerX(), i.getCoordonnerY()));
			}

			if (i.getTypeblock() == Block.typeSuperPacGomme) {
				pacgommes.add(new SuperPacGomme(i.getCoordonnerX(), i.getCoordonnerY()));
			}

		}

	}
	/**
	 * Cette methode affiche un texte au centre de l'editeur
	 * @param Graphics
	 * @param text
	 * @throws FontFormatException
	 * @throws IOException
	 */
	private void affichecenter(Graphics g, String text) throws FontFormatException, IOException {
		fontTexte = Font.createFont(Font.TRUETYPE_FONT, new File("data/assets/fonts/emulogic.ttf")).deriveFont(10f);
		FontMetrics metrics = g.getFontMetrics(this.fontTexte);
		Rectangle rect = new Rectangle(this.getWidth(), this.getHeight());
		int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
		int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
		g.setColor(Color.white);
		g.setFont(this.fontTexte);
		g.drawString(text, x, y);
	}
	@Override
	/**
	 * Cette methode affiche l'editeur
	 * @param Graphics
	 */
	public void paint(Graphics g) {

		super.paint(g);
		for (Block i : map.getMapCollection()) {
			try {
				i.afficher(g, this.getWidth(), this.getHeight());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (PacGomme i : pacgommes) {
			try {
				i.afficher(g, this.getWidth(), this.getHeight());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(erreur != null) {
			try {
				affichecenter(g,erreur);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		this.add(sauvegarde);
		this.add(quitter);
		this.add(mur);
		this.add(couloir);
		this.add(pacGommeFruit);
		this.add(superPacGomme);
		this.setBackground(Color.black);
	}
	/**
	 * Cette methode modifie un Block
	 * @param x
	 * @param y
	 * @param type
	 */
	public void modificationBlock(int x, int y, int type) {
		Block supp = null;
		try {
			for (Block i : map.getMapCollection()) {
				if (i.getCoordonnerX() == x && i.getCoordonnerY() == y) {
					blockInterditModif(i);
					supp = i;

					i = Block.newBlock(x, y, type);

				}
			}
			Block block = Block.newBlock(x, y, type);
			blockInterditModif(block);
			map.getMapCollection().add(block);
			map.getMapCollection().remove(supp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
/**
 * Cette methode verifie si il est autoris� de modifier le bloc pass� en parametre
 * @param modifier
 * @throws Exception
 */
	public void blockInterditModif(Block modifier) throws Exception {
		if (modifier.getCoordonnerX() <= 0 || modifier.getCoordonnerX() >= Map.longueurMap || modifier.getCoordonnerY() <= 0 || modifier.getCoordonnerY() >= Map.largeurMap) {
			erreur = "Vous ne pouvez pas placer de bloc sur la bordure";
			throw new Exception("Erreur : Block spawn ou bordure");
		} else {
			erreur = null;
		}
		Block[][] voisin = map.voisin((int) modifier.getCoordonnerX(), (int) modifier.getCoordonnerY());
		for (Block[] i : voisin) {
			for (Block j : i) {
				if(j.getTypeblock() == Block.typeSpawn) {
					erreur = "Vous ne pouvez pas placer de bloc sur le spawn";
					throw new Exception("Erreur : Block spawn");
				}
			}
		}

	}
	/**
	 * Cette methode sauvegarde la Map
	 * @return Map
	 * @throws Exception
	 */
	public Map saveModificationMap() throws Exception {
		Block[][] tmp = this.map.convertirColectionToTab();
		this.map.getMapCollection().clear();
		for (Block[] i : tmp) {
			for (Block j : i) {
				this.map.getMapCollection().add(j);
			}
		}

		niveauJouer.setMap(this.map);
		FichierCharger.modifierNiveau(niveauJouer);
		close();
		return map;
	}
	public void close() {
		Platform.runLater(new Runnable() {
			public void run() {
				Main.fermerEditeur();

			}
		});
	}
	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX() / ElementGraphique.taille
				- (this.getWidth() / 2 - Main.tailleJeuxX / 2) / ElementGraphique.taille;
		int y = (e.getY() / ElementGraphique.taille
				- (this.getHeight() / 2 - Main.tailleJeuxY / 2) / ElementGraphique.taille)-1;

		System.out.println("case " + x + " " + y);
		if (x < Map.longueurMap && y < Map.largeurMap) {
			try {
				modificationBlock(x, y, selectionBlock);
				map.orientationMurMap();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			creationPacGomme();
			repaint();
		}

	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
