package fr.pacman.background.partie;

/**
 * Cette Classe met en place l'algotithme de Dijkstra, qui gere les deplacements de certain 
 * fantome et ainsi pouvoir piege le joueur. (Pour les detail de l'explication cf le compte rendue).
 * @author mathe
 */

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ImageIcon;

import fr.pacman.zonedejeux.decor.Block;
import fr.pacman.zonedejeux.decor.Map;

public class Dijkstra {
	public static ArrayList<Block> sommet;
	private static Integer[][] adj = null;
	static Map map;

	/**
	 * Constructeur Dijkstra
	 * @param map
	 */
	public Dijkstra(Map map) {
		Dijkstra.map = map;
		Dijkstra.adj = convertireMatriceDAjacence(map);

		// ArrayList<Integer> point = cheminLePlusCour(1, 60);

	}

	/**
	 * Cette methode calcule le chemins le plus court pour arriver au dernier sommet pointer par le
	 * PacMan.
	 * @param a
	 * @param b
	 * @return Le futur sommet sommet du Fantome.
	 */
	public static ArrayList<Integer> cheminLePlusCour(int a, int b) {
		ArrayList<Integer> futureSommet = new ArrayList<>();

		ArrayList<Integer> sommetNonMarque = new ArrayList<>();
		ArrayList<Integer> sommetMarque = new ArrayList<>();

		ArrayList<Integer> distanceDeA = new ArrayList<>();
		ArrayList<Integer> sonPredecesseur = new ArrayList<>();

		for (int i = 0; i < adj.length; i++) {
			sommetNonMarque.add(i);
			sonPredecesseur.add(-1);

			distanceDeA.add(adj[a][i]);

		}

		while (!sommetMarque.contains(b)) {
			int min = 100;
			for (Integer i : sommetNonMarque) {

				if (distanceDeA.get(i) != null) {
					if (min > distanceDeA.get(i)) {
						min = distanceDeA.get(i);
					}
				}
			}

			Integer sommet = -1;

			for (Integer i : sommetNonMarque) {
				if (distanceDeA.get(i) != null && distanceDeA.get(i) == min) {
					sommet = i;
				}
			}

			sommetMarque.add(sommet);
			sommetNonMarque.remove(sommet);

			for (int i = 0; i < adj.length; i++) {

				if ((adj[sommet][i] != null) && (sommet != i)) {
					if (distanceDeA.get(i) != null) {
						if (distanceDeA.get(i) >= distanceDeA.get(sommet) + adj[sommet][i]) {

							distanceDeA.set(i, adj[sommet][i] + distanceDeA.get(sommet));
							sonPredecesseur.set(i, sommet);
						}
					} else {
						distanceDeA.set(i, adj[sommet][i] + distanceDeA.get(sommet));
						sonPredecesseur.set(i, sommet);
					}
				}
			}

		}

		/*
		 * System.out.println(this); System.out.println(distanceDeA);
		 * System.out.println(sonPredecesseur);
		 */

		int i = b;

		while (i != a) {

			futureSommet.add(0, i);
			i = sonPredecesseur.get(i);
		}
		futureSommet.add(0, a);

		System.out.println(futureSommet);
		/*try {
			map.orientationMurMap();
		} catch (BlockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		trancerchemin(futureSommet);*/

		return futureSommet;
	}

	/**
	 * Cette methode trace le chemins que pendra le fantome.
	 * @param pointPassage
	 */
	public static void trancerchemin(ArrayList<Integer> pointPassage) {
		Block[][] mapblock = map.copieMap();
		Block pointAvant = sommet.get(pointPassage.get(0));

		for (Integer i : pointPassage) {

			Block lePointApres = sommet.get(i);
			if (pointAvant.getCoordonnerX() == lePointApres.getCoordonnerX()) {
				for (int j = (int) Math.min(pointAvant.getCoordonnerY(), lePointApres.getCoordonnerY()); j < Math
						.max(pointAvant.getCoordonnerY(), lePointApres.getCoordonnerY()); j++) {
					mapblock[j][(int) pointAvant.getCoordonnerX()]
							.setTexture(new ImageIcon("./data/assets/images/spawn.png").getImage());
				}
			} else if (pointAvant.getCoordonnerY() == lePointApres.getCoordonnerY()) {
				for (int j = (int) Math.min(pointAvant.getCoordonnerX(), lePointApres.getCoordonnerX()); j < Math
						.max(pointAvant.getCoordonnerX(), lePointApres.getCoordonnerX()); j++) {
					mapblock[(int) pointAvant.getCoordonnerY()][j]
							.setTexture(new ImageIcon("./data/assets/images/spawn.png").getImage());
				}
			}

			pointAvant = lePointApres;

		}

	}

	/**
	 * Cette methode est tres important car elle permet de mettre sous forme de tableau la map de niveau
	 * lancer, et ainsi avoir les sommets et les chemins de la carte.
	 * @param map
	 * @return La matrice d'adjacence de la map.
	 */
	private Integer[][] convertireMatriceDAjacence(Map map) {
		sommet = new ArrayList<>();

		for (Block i : map.getMapCollection()) {
			Collection<Integer> possible = null;
			if (i.getTypeblock() >= Block.typeCouloir) {
				possible = sommetPossible(map.voisin((int) i.getCoordonnerX(), (int) i.getCoordonnerY()));

				if (possible.size() == 1 || possible.size() >= 3
						|| (i.getCoordonnerX() == 15 && i.getCoordonnerY() == 10)) {
					sommet.add(i);
					// i.setTexture(new
					// ImageIcon("./data/assets/images/pacgommefruitblock.png").getImage());
				}
				if (possible.size() == 2) {
					if (!((possible.contains(1) && possible.contains(3))
							|| (possible.contains(2) && possible.contains(4)))) {
						sommet.add(i);
						// i.setTexture(new
						// ImageIcon("./data/assets/images/pacgommefruitblock.png").getImage());
					}
				}
			}
		}

		Block[][] mapBlock = map.copieMap();

		Dijkstra.adj = new Integer[sommet.size()][sommet.size()];

		for (int a = 0; a < sommet.size(); a++) {
			for (int b = 0; b < sommet.size(); b++) {

				if (sommet.get(b).getCoordonnerX() == sommet.get(a).getCoordonnerX()) {
					boolean continuite = true;
					for (int j = 0; j <= sommet.get(b).getCoordonnerY() - sommet.get(a).getCoordonnerY(); j++) {

						if (mapBlock[(int) sommet.get(a).getCoordonnerY() + j][(int) sommet.get(a).getCoordonnerX()]
								.getTypeblock() < Block.typeCouloir) {

							continuite = false;
						}
					}

					if (continuite && (sommet.get(b).getCoordonnerY() - sommet.get(a).getCoordonnerY() >= 0)) {
						adj[a][b] = (int) (sommet.get(b).getCoordonnerY() - sommet.get(a).getCoordonnerY());
						adj[b][a] = (int) (sommet.get(b).getCoordonnerY() - sommet.get(a).getCoordonnerY());
					}

				}

				if (sommet.get(b).getCoordonnerY() == sommet.get(a).getCoordonnerY()) {
					boolean continuite = true;
					for (int j = 0; j <= sommet.get(b).getCoordonnerX() - sommet.get(a).getCoordonnerX(); j++) {

						if (mapBlock[(int) sommet.get(a).getCoordonnerY()][(int) sommet.get(a).getCoordonnerX() + j]
								.getTypeblock() < Block.typeCouloir) {
							continuite = false;
						}
					}

					if (continuite && (sommet.get(b).getCoordonnerX() - sommet.get(a).getCoordonnerX() >= 0)) {
						adj[a][b] = (int) (sommet.get(b).getCoordonnerX() - sommet.get(a).getCoordonnerX());
						adj[b][a] = (int) (sommet.get(b).getCoordonnerX() - sommet.get(a).getCoordonnerX());
					}

				}

			}

		}

		return adj;
	}

	/**
	 * @param tabvoisin
	 * @return le tableau des sommet que peut prendre le fantome.
	 */
	public Collection<Integer> sommetPossible(Block[][] tabvoisin) {
		Collection<Integer> tab = new ArrayList<>();

		if (tabvoisin[1][0].getTypeblock() > Block.typeSpawn) {
			tab.add(1);
		}
		if (tabvoisin[0][1].getTypeblock() > Block.typeSpawn) {
			tab.add(2);
		}
		if (tabvoisin[1][2].getTypeblock() > Block.typeSpawn) {
			tab.add(3);
		}
		if (tabvoisin[2][1].getTypeblock() > Block.typeSpawn) {
			tab.add(4);
		}

		return tab;
	}

	/**
	 * Pas compris ce que fait cette methode :/
	 */
	@Override
	public String toString() {
		String str = "";
		for (Integer[] i : adj) {
			for (Integer j : i) {
				if (j == null) {
					str += String.format("%3d", -1);
				} else {
					str += String.format("%3d", j);
				}
			}
			str += "\n";
		}

		return str;
	}
}
