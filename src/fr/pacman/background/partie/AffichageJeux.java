package fr.pacman.background.partie;

/**
 * Cette Classe gere l'affiche du jeu lors du lancement du programme.
 * @author mathe
 */

import javax.swing.JFrame;

import fr.exceptionpersonnalisee.BlockException;
import fr.pacman.background.gestion.FichierCharger;

@SuppressWarnings("serial")
public class AffichageJeux extends JFrame {
	Partie panel;

	/**
	 * Constructeur de AfficherJeux
	 * @throws BlockException
	 */
	public AffichageJeux() throws BlockException {
		super("PAC MAN");
		setResizable(true);
		panel = new Partie(FichierCharger.getUnNiveau("N0000000099").getId());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(panel);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
