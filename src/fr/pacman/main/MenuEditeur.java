package fr.pacman.main;

import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderStroke;
import javafx.geometry.Pos;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Font;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;
import javafx.geometry.Side;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.animation.ScaleTransition;
import java.awt.Dimension;
import javafx.scene.paint.Paint;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import java.awt.Toolkit;
import javax.sound.sampled.Clip;
import javafx.scene.control.Button;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MenuEditeur extends Stage {
	private Text titre;
	private Text titre2;
	private TextFlow titreFlow;
	private Button nouveau;
	private Button charger;
	private Button retour;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private ImageView ecran = Fenetre.imageEcran;
	private ImageView son = Fenetre.imageSon;
	
	private Clip hover = SoundLoader.loadSound("MenuHover.wav");
	private Clip select = SoundLoader.loadSound("MenuSelect.wav");

	Scene scene;

	public MenuEditeur() {
        this.titre = new Text("PAC");
        this.titre2 = new Text("man");
        this.titreFlow = new TextFlow();
        this.nouveau = new Button("NOUVEAU");
        this.charger = new Button("CHARGER");
        this.retour = new Button("RETOUR");
        
        this.setTitle("PacMan");
        this.setMinHeight(625.0);
        this.setMinWidth(775.0);
        this.setX(0.0);
        this.setY(50.0);
        this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.WHITE);
        this.setFullScreen(true);
        this.setScene(scene);
        this.sizeToScene();
    }
    
    private ScaleTransition scaleIn(final Button btn) {
        final ScaleTransition StIn = new ScaleTransition();
        StIn.setNode((Node)btn);
        StIn.setToX(1.1);
        StIn.setToY(1.1);
        StIn.setDuration(new Duration(50.0));
        StIn.setCycleCount(1);
        return StIn;
    }
    
    private ScaleTransition scaleOut(final Button btn) {
        final ScaleTransition StOut = new ScaleTransition();
        StOut.setNode((Node)btn);
        StOut.setToX(1.0);
        StOut.setToY(1.0);
        StOut.setDuration(new Duration(50.0));
        StOut.setCycleCount(1);
        return StOut;
    }
    
    public synchronized void startClip(Clip clip) {
        clip.stop();
        clip.setFramePosition(0);
        clip.start();
    }
    
    private void gererClick(final ActionEvent e) {
        if (e.getSource() == this.nouveau) {
        	startClip(select);
        	Fenetre.editeurSaisie();
        }
        else if (e.getSource() == this.charger) {
        	startClip(select);
        	Fenetre.editeurNiveau();
        }
        else if(e.getSource() == this.retour) {
        	startClip(select);
        	Fenetre.retourMenu();
        }
    }
    
    public BorderPane creerContenu() {
        final BorderPane root = new BorderPane();
        root.setBackground(new Background(new BackgroundImage[] { new BackgroundImage(new Image("file:data/assets/images/FondPacMan.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, new BackgroundPosition(Side.LEFT, 0.0, true, Side.BOTTOM, 0.0, true), new BackgroundSize(-1.0, -1.0, true, true, false, true)) }));
        
        final Font Pacfont = Font.loadFont("file:data/assets/fonts/PAC-FONT.TTF", 130.0);
        final Font emulogic = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 35.0);
        
        this.titreFlow.getChildren().addAll(titre, titre2);
        this.titre.setFill((Paint)Color.YELLOW);
        this.titre.setFont(Pacfont);
        this.titre2.setFill((Paint)Color.RED);
        this.titre2.setFont(Pacfont);
        this.titreFlow.setTextAlignment(TextAlignment.CENTER);
        
        son.setFitHeight(75);
		son.setFitWidth(80);
		son.setPreserveRatio(true);
		son.setOnMouseClicked(e -> Fenetre.muteMusic());
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		
		
		ecran.setFitHeight(75);
		ecran.setFitWidth(80);
		ecran.setPreserveRatio(true);
		
		ecran.setOnMouseClicked(e -> Fenetre.ToggleFullScreen());
		
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		HBox imageBox = new HBox(20.0);
		imageBox.getChildren().addAll(son, ecran);
		root.setBottom(imageBox);
        
        this.nouveau.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
        this.nouveau.setTextFill((Paint)Color.WHITE);
        this.nouveau.setMaxWidth(400.0);
        this.nouveau.setFont(emulogic);
        this.nouveau.setStyle("-fx-background-color: transparent;");
        this.nouveau.setScaleX(1.0);
        this.nouveau.setScaleY(1.0);
        this.nouveau.setOnMouseEntered(e -> {this.scaleIn(this.nouveau).playFromStart();startClip(hover);});
        this.nouveau.setOnMouseExited(e -> this.scaleOut(this.nouveau).playFromStart());
        this.nouveau.setOnAction(e -> this.gererClick(e));
        
        this.charger.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
        this.charger.setTextFill((Paint)Color.WHITE);
        this.charger.setMaxWidth(400.0);
        this.charger.setFont(emulogic);
        this.charger.setStyle("-fx-background-color: transparent;");
        this.charger.setScaleX(1.0);
        this.charger.setScaleY(1.0);
        this.charger.setOnMouseEntered(e -> {this.scaleIn(this.charger).playFromStart();startClip(hover);});
        this.charger.setOnMouseExited(e -> this.scaleOut(this.charger).playFromStart());
        this.charger.setOnAction(e -> this.gererClick(e));
        
        this.retour.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
        this.retour.setTextFill((Paint)Color.WHITE);
        this.retour.setMaxWidth(400.0);
        this.retour.setFont(emulogic);
        this.retour.setStyle("-fx-background-color: transparent;");
        this.retour.setScaleX(1.0);
        this.retour.setScaleY(1.0);
        this.retour.setOnMouseEntered(e -> {this.scaleIn(this.retour).playFromStart();startClip(hover);});
        this.retour.setOnMouseExited(e -> this.scaleOut(this.retour).playFromStart());
        this.retour.setOnAction(e -> this.gererClick(e));
        
  
        final VBox vbox = new VBox(50.0);
        vbox.getChildren().addAll(this.titreFlow);
        VBox.setMargin((Node)this.titreFlow, new Insets(60.0, 0.0, 0.0, 0.0));
        final VBox BoxButton = new VBox(30.0);
        BoxButton.getChildren().addAll(this.nouveau, this.charger, this.retour);
        vbox.setAlignment(Pos.CENTER);
        BoxButton.setAlignment(Pos.CENTER);
        root.setTop((Node)vbox);
        root.setCenter((Node)BoxButton);
        return root;
    }
}
