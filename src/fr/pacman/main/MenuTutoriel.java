package fr.pacman.main;

import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderStroke;
import javafx.geometry.Pos;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Font;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;
import javafx.geometry.Side;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.animation.ScaleTransition;
import java.awt.Dimension;
import javafx.scene.paint.Paint;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import java.awt.Toolkit;
import javax.sound.sampled.Clip;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MenuTutoriel extends Stage {
	private Text titre;
	private Text titre2;
	private TextFlow titreFlow;
	private Text objectif;
	private Text but;
	private Text commande;
	private Text pauseT;
	private Text toucheT;
	private ImageView touches = new ImageView(new Image("file:data/assets/images/Commande.png"));
	private ImageView pause = new ImageView(new Image("file:data/assets/images/Escape.png"));
	private ImageView ecran = Fenetre.imageEcran;
	private Button retour;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private Clip hover = SoundLoader.loadSound("MenuHover.wav");
	private Clip select = SoundLoader.loadSound("MenuSelect.wav");
	private ImageView son = Fenetre.imageSon;

	Scene scene;

	public MenuTutoriel() {
		this.titre = new Text("PAC");
		this.titre2 = new Text("man");
		this.but = new Text("But du jeu :\n\n Manger tout les Pacgommes du niveau sans epuiser toute ses vies\n");
		this.objectif = new Text("Objectifs :\n\n - Manger tout les PacGommes\n - Ne pas perdre toutes ses vies\n - Faire le plus grand score possible en un minimum de temps\n");
		this.titreFlow = new TextFlow();
		this.commande = new Text("Les touches :");
		this.pauseT = new Text("Pause :");
		this.toucheT = new Text("Bouger :");
		this.retour = new Button("RETOUR");

		this.setTitle("PacMan");
		this.setMinHeight(625.0);
		this.setMinWidth(775.0);
		this.setX(0.0);
		this.setY(50.0);
		this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.WHITE);
		this.setFullScreen(true);
		this.scene.getStylesheets().add(getClass().getResource("liststyle.css").toExternalForm());
		this.setScene(scene);
		this.sizeToScene();
	}

	private ScaleTransition scaleIn(final Button btn) {
		final ScaleTransition StIn = new ScaleTransition();
		StIn.setNode((Node) btn);
		StIn.setToX(1.1);
		StIn.setToY(1.1);
		StIn.setDuration(new Duration(50.0));
		StIn.setCycleCount(1);
		return StIn;
	}

	private ScaleTransition scaleOut(final Button btn) {
		final ScaleTransition StOut = new ScaleTransition();
		StOut.setNode((Node) btn);
		StOut.setToX(1.0);
		StOut.setToY(1.0);
		StOut.setDuration(new Duration(50.0));
		StOut.setCycleCount(1);
		return StOut;
	}

	public synchronized void startClip(Clip clip) {
		clip.stop();
		clip.setFramePosition(0);
		clip.start();
	}

	private void gererClick(final ActionEvent e) {
		if (e.getSource() == this.retour) {
			startClip(select);
			Fenetre.retourMenu();
		}
	}

	public BorderPane creerContenu() {

		final BorderPane root = new BorderPane();
		root.setBackground(new Background(
				new BackgroundImage[] { new BackgroundImage(new Image("file:data/assets/images/FondPacMan.png"),
						BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT,
						new BackgroundPosition(Side.LEFT, 0.0, true, Side.BOTTOM, 0.0, true),
						new BackgroundSize(-1.0, -1.0, true, true, false, true)) }));

		final Font Pacfont = Font.loadFont("file:data/assets/fonts/PAC-FONT.TTF", 130.0);
		final Font emulogic = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 35.0);
		final Font emulogicP = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 15.0);

		this.titreFlow.getChildren().addAll(titre, titre2);
		this.titre.setFill((Paint) Color.YELLOW);
		this.titre.setFont(Pacfont);
		this.titre2.setFill((Paint) Color.RED);
		this.titre2.setFont(Pacfont);
		this.titreFlow.setTextAlignment(TextAlignment.CENTER);
		
		this.but.setFill((Paint) Color.WHITE);
		this.but.setFont(emulogicP);
		
		this.objectif.setFill((Paint) Color.WHITE);
		this.objectif.setFont(emulogicP);
		
		this.commande.setFill((Paint) Color.WHITE);
		this.commande.setFont(emulogicP);
		
		this.pauseT.setFill((Paint) Color.WHITE);
		this.pauseT.setFont(emulogicP);
		
		this.toucheT.setFill((Paint) Color.WHITE);
		this.toucheT.setFont(emulogicP);
		
		pause.setFitHeight(50);
		pause.setFitWidth(50);
		pause.setPreserveRatio(true);
		
		touches.setFitHeight(104);
		touches.setFitWidth(156);
		touches.setPreserveRatio(true);

		son.setFitHeight(75);
		son.setFitWidth(80);
		son.setPreserveRatio(true);
		son.setOnMouseClicked(e -> Fenetre.muteMusic());
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		
		
		ecran.setFitHeight(75);
		ecran.setFitWidth(80);
		ecran.setPreserveRatio(true);
		
		ecran.setOnMouseClicked(e -> Fenetre.ToggleFullScreen());
		
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		HBox imageBox = new HBox(20.0);
		imageBox.getChildren().addAll(son, ecran);
		root.setBottom(imageBox);

		this.retour.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.retour.setTextFill((Paint) Color.WHITE);
		this.retour.setMaxWidth(400.0);
		this.retour.setFont(emulogic);
		this.retour.setStyle("-fx-background-color: transparent;");
		this.retour.setScaleX(1.0);
		this.retour.setScaleY(1.0);
		this.retour.setOnMouseEntered(e -> {
			this.scaleIn(this.retour).playFromStart();
			startClip(hover);
		});
		this.retour.setOnMouseExited(e -> this.scaleOut(this.retour).playFromStart());
		this.retour.setOnAction(e -> this.gererClick(e));
		
		final HBox pauseBox = new HBox();
		pauseBox.setAlignment(Pos.CENTER);
		pauseBox.getChildren().addAll(this.pauseT, this.pause);
		HBox.setMargin(this.pauseT, new Insets(60.0, 700.0, 0.0, 0.0));
		
		final HBox toucheBox = new HBox();
		toucheBox.setAlignment(Pos.CENTER);
		toucheBox.getChildren().addAll(this.toucheT, this.touches);
		HBox.setMargin(this.toucheT, new Insets(60.0, 600.0, 0.0, 0.0));
		
		final VBox textBox = new VBox();
		textBox.getChildren().addAll(this.but, this.objectif, this.commande,toucheBox, pauseBox);
		textBox.setAlignment(Pos.CENTER_LEFT);
		textBox.setPadding(new Insets(20));
		textBox.setMaxWidth(this.getWidth()-200);
		textBox.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		textBox.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint) Color.BLUE,
				BorderStrokeStyle.SOLID, new CornerRadii(50.0), new BorderWidths(7.0)) }));
		
		final VBox vbox = new VBox(50.0);
		vbox.getChildren().addAll(this.titreFlow);
		vbox.setAlignment(Pos.CENTER);
		VBox.setMargin((Node) this.titreFlow, new Insets(60.0, 0.0, 0.0, 0.0));
		
		final VBox BoxButton = new VBox(30.0);
		BoxButton.getChildren().addAll(textBox, this.retour);
		BoxButton.setAlignment(Pos.CENTER);
		
		root.setTop((Node) vbox);
		root.setCenter((Node) BoxButton);

		return root;
	}
}
