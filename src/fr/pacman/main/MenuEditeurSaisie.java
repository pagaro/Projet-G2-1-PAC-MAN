package fr.pacman.main;

import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderStroke;
import javafx.geometry.Pos;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Font;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;
import javafx.geometry.Side;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.Node;
import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import java.awt.Dimension;
import javafx.scene.paint.Paint;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import java.awt.Toolkit;
import javax.sound.sampled.Clip;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MenuEditeurSaisie extends Stage {
	private Text titre;
	private Text titre2;
	private TextFlow titreFlow;
	private Label label;
	private Label erreur;
	private Button creer;
	private Button annuler;
	private TextField saisie;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private ImageView ecran = Fenetre.imageEcran;
	private ImageView son = Fenetre.imageSon;
	private String nomNiveau;
	
	private Clip hover = SoundLoader.loadSound("MenuHover.wav");
	private Clip select = SoundLoader.loadSound("MenuSelect.wav");

	Scene scene;

	public MenuEditeurSaisie() {
        this.titre = new Text("PAC");
        this.titre2 = new Text("man");
        this.titreFlow = new TextFlow();
        this.creer = new Button("creer");
        this.annuler = new Button("annuler");
        this.saisie = new TextField();
        this.label = new Label("Saisir le nom");
        this.erreur = new Label("la saisie est vide");
        this.erreur.setVisible(false);
        this.saisie.prefWidth(600.0);
        
        
        this.setTitle("PacMan");
        this.setMinHeight(625.0);
        this.setMinWidth(775.0);
        this.setX(0.0);
        this.setY(50.0);
        this.scene = new Scene(creerContenu(), dim.width, dim.height, Color.WHITE);
        this.setFullScreen(true);
        this.setScene(scene);
        this.sizeToScene();
    }
    
    private ScaleTransition scaleIn(final Button btn) {
        final ScaleTransition StIn = new ScaleTransition();
        StIn.setNode((Node)btn);
        StIn.setToX(1.1);
        StIn.setToY(1.1);
        StIn.setDuration(new Duration(50.0));
        StIn.setCycleCount(1);
        return StIn;
    }
    
    private ScaleTransition scaleOut(final Button btn) {
        final ScaleTransition StOut = new ScaleTransition();
        StOut.setNode((Node)btn);
        StOut.setToX(1.0);
        StOut.setToY(1.0);
        StOut.setDuration(new Duration(50.0));
        StOut.setCycleCount(1);
        return StOut;
    }
    
    public synchronized void startClip(Clip clip) {
        clip.stop();
        clip.setFramePosition(0);
        clip.start();
    }
    
    private void gererClick(final ActionEvent e) {
        if (e.getSource() == this.creer) {
        	startClip(select);
        	if(!this.saisie.getText().trim().isEmpty()) {
        		this.erreur.setVisible(false);
        		this.nomNiveau = this.saisie.getText();
        		Main.lancerEditeurNouveau(nomNiveau);
        	}else {
        		this.erreur.setVisible(true);
        	}
        }
        else if(e.getSource() == this.annuler) {
        	startClip(select);
        	Fenetre.editeur();
        }
    }
    
    public static void addTextLimiter(final TextField tf, final int maxLength) {
        tf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                if (tf.getText().length() > maxLength) {
                    String s = tf.getText().substring(0, maxLength);
                    tf.setText(s);
                }
            }
        });
    }
    
    public BorderPane creerContenu() {
        final BorderPane root = new BorderPane();
        root.setBackground(new Background(new BackgroundImage[] { new BackgroundImage(new Image("file:data/assets/images/FondPacMan.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, new BackgroundPosition(Side.LEFT, 0.0, true, Side.BOTTOM, 0.0, true), new BackgroundSize(-1.0, -1.0, true, true, false, true)) }));
        
        final Font Pacfont = Font.loadFont("file:data/assets/fonts/PAC-FONT.TTF", 130.0);
        final Font emulogic = Font.loadFont("file:data/assets/fonts/emulogic.ttf", 35.0);
        
        this.titreFlow.getChildren().addAll(titre, titre2);
        this.titre.setFill((Paint)Color.YELLOW);
        this.titre.setFont(Pacfont);
        this.titre2.setFill((Paint)Color.RED);
        this.titre2.setFont(Pacfont);
        this.titreFlow.setTextAlignment(TextAlignment.CENTER);
        this.label.setTextFill((Paint)Color.WHITE);
        this.label.setFont(emulogic);
        this.erreur.setFont(emulogic);
        this.erreur.setTextFill((Paint)Color.RED);
        addTextLimiter(this.saisie, 15);
        
        son.setFitHeight(75);
		son.setFitWidth(80);
		son.setPreserveRatio(true);
		son.setOnMouseClicked(e -> Fenetre.muteMusic());
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		this.saisie.setFont(emulogic);
		this.saisie.setMaxWidth(600.0);
		this.saisie.setStyle("-fx-background-color: transparent;-fx-text-inner-color: white;");
		this.saisie.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
		this.saisie.setOnKeyReleased(e->checkVide());
		
		ecran.setFitHeight(75);
		ecran.setFitWidth(80);
		ecran.setPreserveRatio(true);
		
		ecran.setOnMouseClicked(e -> Fenetre.ToggleFullScreen());
		
		BorderPane.setAlignment(this.titreFlow, Pos.TOP_CENTER);
		
		HBox imageBox = new HBox(20.0);
		imageBox.getChildren().addAll(son, ecran);
		root.setBottom(imageBox);
        
        this.creer.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
        this.creer.setTextFill((Paint)Color.WHITE);
        this.creer.setMaxWidth(400.0);
        this.creer.setFont(emulogic);
        this.creer.setStyle("-fx-background-color: transparent;");
        this.creer.setScaleX(1.0);
        this.creer.setScaleY(1.0);
        this.creer.setOnMouseEntered(e -> {this.scaleIn(this.creer).playFromStart();startClip(hover);});
        this.creer.setOnMouseExited(e -> this.scaleOut(this.creer).playFromStart());
        this.creer.setOnAction(e -> this.gererClick(e));
       
        
        this.annuler.setBorder(new Border(new BorderStroke[] { new BorderStroke((Paint)Color.BLUE, BorderStrokeStyle.SOLID, new CornerRadii(100.0), new BorderWidths(7.0)) }));
        this.annuler.setTextFill((Paint)Color.WHITE);
        this.annuler.setMaxWidth(400.0);
        this.annuler.setFont(emulogic);
        this.annuler.setStyle("-fx-background-color: transparent;");
        this.annuler.setScaleX(1.0);
        this.annuler.setScaleY(1.0);
        this.annuler.setOnMouseEntered(e -> {this.scaleIn(this.annuler).playFromStart();startClip(hover);});
        this.annuler.setOnMouseExited(e -> this.scaleOut(this.annuler).playFromStart());
        this.annuler.setOnAction(e -> this.gererClick(e));
        
  
        final VBox vbox = new VBox(50.0);
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(this.titreFlow);
        VBox.setMargin((Node)this.titreFlow, new Insets(60.0, 0.0, 0.0, 0.0));
        
        final VBox center = new VBox(50.0);
        center.setAlignment(Pos.CENTER);
        
        final HBox BoxButton = new HBox(30.0);
        BoxButton.getChildren().addAll(this.creer, this.annuler);
        BoxButton.setAlignment(Pos.CENTER);
        
        center.getChildren().setAll(this.label, this.saisie, this.erreur,BoxButton);
        
        root.setTop(vbox);
        root.setCenter(center);
        
        return root;
    }

    private void checkVide() {
    	if (this.saisie.getText().isEmpty())
    	{
    		this.erreur.setVisible(true);
    	}
    	else {
    		this.erreur.setVisible(false);
    	}
	}

	public String getNomNiveauSaisie() {
    	return this.nomNiveau;
    }
}
