package fr.pacman.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.pacman.background.partie.Partie;

class PartieTest {

	@Test
	public void Testinitialisation() {
		Partie p = new Partie("N0000000001");
		
		//assertEquals(-1, p.initialisation("N0000000099"));		 
		assertEquals(0, p.initialisation("N0000000001"));		 
		//assertEquals(-2, p.initialisation("N0000000099"));		 
	}

}
