package fr.pacman.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.exceptionpersonnalisee.GrosProblemeFichierException;
import fr.pacman.background.gestion.LectureFichierXML;
import fr.pacman.background.partie.Dijkstra;
import fr.pacman.zonedejeux.decor.Map;
import fr.pacman.zonedejeux.decor.Niveau;

class DijkstraTest {

	@SuppressWarnings("static-access")
	@Test
	public void TestCheminCour() {
		Niveau niveau = null;
		boolean ok = true;
		try {
			niveau = LectureFichierXML.lireNiveau("./data/niveau/defaut/niveauDijkstra.pac");	
		} catch (GrosProblemeFichierException e) {
			ok = false;
			System.out.println(e);
		}

		if (ok){
			Map m = new Map();

			m = niveau.getMap();
			Dijkstra dij = new Dijkstra(m);

			ArrayList<Integer> list = new ArrayList<>();
			list.add(3);
			list.add(2);
			list.add(0);

			assertEquals(list, dij.cheminLePlusCour(3, 0));
			
		}

	}

}
